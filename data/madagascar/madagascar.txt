1.Madagascar dry deciduous forests
The composition of the wildlife of Madagascar reflects the fact that the island has been isolated for about 88 million years.

2. Kirindy National Park
The Avenue or Alley of the Baobabs is a prominent group of baobab trees lining the dirt road between Morondava and Belon'i Tsiribihina in the Menabe region in western Madagascar.

3.Andilana Beach Resort
Andilana Beach Resort, nestled in the beautiful island of Nosy Be, is a luxury hotel in Madagascar, a dreamy destination ideal for demanding travellers.

4.Giraffe Weevil
The giraffe weevil is a weevil endemic to Madagascar. It derives its name from an extended neck much like that of the common giraffe.