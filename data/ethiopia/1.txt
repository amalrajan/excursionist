1. Rock-Hewn Churches, Lalibela- UNESCO World Heritage Sites in Ethiopia

The 11 medieval monolithic cave churches of this 13th-century 'New Jerusalem' are situated in a mountainous
 region in the heart of Ethiopia near a traditional village with circular-shaped dwellings. Lalibela is a high
 place of Ethiopian Christianity, still today a place of pilmigrage and devotion.

The Blue Nile Fall

The Blue Nile, the longest river in Africa falls into a canyon to form one of the most spectacular waterfalls
 in the continent. The 150 feet high massive waterfall gush downward creating a cloud of vapor which is called 
Tisisat- (smoking fire). The four streams of the Blue Nile creates enormous waterfall. Lake Tana, the source 
of Blue Nile River which meets the White Nile River in neighboring Sudan supplying 85% of water to Great Nile 
River, forms a spectacular fall.

Timkat Celebration

Timkat is the Ethiopian Orthodox celebration of Epiphany. It is celebrated on January 19 (or 20 on Leap Year), 
corresponding to the 10th day of Terr following the Ethiopian calendar. Timkat celebrates the baptism of Jesus
 in the Jordan River. This festival is best known for its ritual reenactment of baptism (similar to such 
reenactments performed by numerous Christian pilgrims to the Holy Land when they visit the Jordan)

Simien Mountains National Park 

Simien Mountains National Park is one of the national parks of Ethiopia. Located in the Semien (North) Gondar 
Zone of the Amhara Region, its territory covers the Semien Mountains and includes Ras Dashan, the highest point 
in Ethiopia. It is home to a number of endangered species, including the Ethiopian wolf and the walia ibex, a 
wild goat found nowhere else in the world. The gelada baboon and the caracal, a cat, also occur within the 
Simien Mountains. More than 50 species of birds inhabit the park, including the impressive bearded vulture, or 
lammergeier, with its 10 foot (3 m) wingspan.