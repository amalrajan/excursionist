# The Excursionist
_The Excursionist is a fun, tangible picture viewer. It allows you to browse through the photos you've taken in a playful way. By touching a country, you can select an album, and then, by spinning the globe back and forth, you can browse your album_

<img src=https://gitlab.com/amalrajan/excursionist/-/raw/master/images/image1.png>
<img src=https://gitlab.com/amalrajan/excursionist/-/raw/master/images/image2.png>

## Requirements
* Arduino Micro
* Breadboard (generic)
* Jumper wires
* Resistor (100k ohm)
* Rotary encoder module
* Capacitor (1 micro farad)
* World globe
* Metal Pins

## Circuit Connection

<img src=https://gitlab.com/amalrajan/excursionist/-/raw/master/images/image3.png>

## Control Flow

<img src=https://gitlab.com/amalrajan/excursionist/-/raw/master/images/image4.png>


## License
`MIT License`
